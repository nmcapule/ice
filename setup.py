#!/usr/bin/env python

from distutils.core import setup

setup(name='Ice',
	version='0.0.0',
	description='Simple chat application plus extras',
	author='Nathaniel M. Capule',
	author_email='nmcapule@gmail.com',
	url='https://bitbucket.org/nmcapule/ice',
	packages=[]
	)