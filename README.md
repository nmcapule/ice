Ice
===
This is a simple chat application designed for easy sharing of code snippets, images, links and whatever's convenient in a workplace.


TODO
----
* Learn sockets/p2p in python
* Learn python conventions. Might want to read up on google style guide if there's any
* Learn python OOP. Might be handy.
* Learn python GUI programming. Low priority. Get me something of a proof of concept first!
* I'll want to attach a license once I have something releaseable