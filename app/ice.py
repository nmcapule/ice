import sys
import threading
import socket
import select
import logging
import time

class Ice(threading.Thread):
	def __init__(self):

class IceServer(threading.Thread):
	def __init__(self, port, host=''):
		threading.Thread.__init__(self)
		self.host = host
		self.port = port
		self.running = True;

	def run(self):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		sock.bind((self.host, self.port))
		sock.listen(1)

		conn, addr = sock.accept()

		logging.info('Server is up')

		while self.running:
			ready_in, ready_out, ready_ex = select.select([conn], [conn], [])
			for item in ready_in:
				data = conn.recv(1024)
				if data:
					print('> ' + data.decode('utf-8'))
				else:
					break
			time.sleep(0)

	def kill(self):
		self.running = False

def main():
	IceServer(1111).start()

if __name__ == "__main__":
	main()